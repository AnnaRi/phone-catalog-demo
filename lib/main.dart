import 'package:catalog/design/theme.dart' as theme;
import 'package:catalog/pages/catalog-list.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: CatalogList(),
    theme: theme.theme,
  ));
}

import 'package:catalog/classes/person.dart';
import 'package:catalog/components/bottom-bar.dart';
import 'package:catalog/components/buttons.dart' as btn;
import 'package:catalog/components/text-fields.dart' as field;
import 'package:catalog/components/images.dart' as img;
import 'package:catalog/design/theme.dart' as theme;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ContactProfile extends StatefulWidget {
  final Person person;

  ContactProfile(this.person);

  @override
  State<StatefulWidget> createState() => _ContactProfileState();
}

class _ContactProfileState extends State<ContactProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _givenName;
  TextEditingController _surName;
  TextEditingController _phone;
  List<int> _imageBytes;

  Person get contact => widget.person;

  @override
  void initState() {
    _givenName = new TextEditingController(text: contact?.givenName ?? '');
    _surName = new TextEditingController(text: contact?.surName ?? '');
    _phone = new TextEditingController(text: contact?.phoneNumber ?? '');
    _imageBytes = contact?.imageBytes;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(contact?.fullName ?? 'Ny kontakt'),
            leading:
                IconButton(icon: Icon(Icons.chevron_left), onPressed: _goBack)),
        body: Form(
            key: _formKey,
            child: Column(children: [
              Expanded(
                  child: SingleChildScrollView(
                      child: Padding(
                          padding: theme.pageMargins,
                          child: Column(children: [
                            InkWell(
                                child: img.profileImage(_imageBytes, 180),
                                onTap: () async {
                                  _imageBytes = await _takePicture();
                                  setState(() {});
                                }),
                            Container(height: theme.spaceBtwItems),
                            field.textFieldWithLabel(_givenName, 'Förnamn',
                                validate: true),
                            field.textFieldWithLabel(_surName, 'Efternamn',
                                validate: true),
                            field.textFieldWithLabel(_phone, 'Telefonnummer',
                                validate: true)
                          ])))),
              bottomBar(context, [
                btn.secondaryButton('Radera', _removeContact),
                btn.primaryButton('Spara', _saveChanges),
              ])
            ])));
  }

  Future<List<int>> _takePicture() async {
    var imagePicker = await ImagePicker().getImage(source: ImageSource.camera);
    return await imagePicker?.readAsBytes();
  }

  void _saveChanges() {
    if (_formKey.currentState.validate()) {
      var updated =
          Person(_givenName.text, _surName.text, _phone.text, _imageBytes);
      Navigator.of(context).pop(updated);
    }
  }

  void _goBack() {
    Navigator.of(context).pop(contact);
  }

  void _removeContact() {
    Navigator.of(context).pop();
  }
}

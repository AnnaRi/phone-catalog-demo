import 'package:catalog/classes/person.dart';
import 'package:catalog/components/images.dart';
import 'package:catalog/pages/contact-profile.dart';
import 'package:flutter/material.dart';

class CatalogList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CalalogListState();
}

class _CalalogListState extends State<CatalogList> {
  final List<Person> contacts = new List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Kontakter')),
        body: ListView.builder(
            itemCount: contacts.length,
            itemBuilder: (BuildContext context, int index) =>
                contactItem(contacts[index], index)),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              _editProfileClick(null, 0);
            },
            child: Icon(Icons.add)));
  }

  Widget contactItem(Person person, int index) {
    return ListTile(
      title: Text(person.fullName),
      leading: profileImage(person.imageBytes, 42),
      trailing: Icon(Icons.chevron_right),
      onTap: () {
        _editProfileClick(person, index);
      },
    );
  }

  void _editProfileClick(Person person, int index) async {
    var updatedProfile = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ContactProfile(person)));
    if (updatedProfile == null && person != null) {
      contacts.removeAt(index);
    } else if (updatedProfile != null) {
      if (person == null) {
        contacts.add(updatedProfile);
      } else {
        contacts[index] = updatedProfile;
      }
    }
    setState(() {});
  }
}

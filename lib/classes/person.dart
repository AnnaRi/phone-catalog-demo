class Person {
  String givenName;
  String surName;
  String phoneNumber;
  List<int> imageBytes;

  Person(this.givenName, this.surName, this.phoneNumber, this.imageBytes);
  Person.noImage(this.givenName, this.surName, this.phoneNumber);

  String get fullName => '$givenName $surName';
  String getFullName() {
    return '$givenName $surName';
  }
}

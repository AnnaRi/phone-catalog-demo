import 'package:catalog/design/theme.dart' as theme;
import 'package:flutter/material.dart';

Widget textField(TextEditingController controller,
    {String hint,
    int maxLines,
    int maxLength,
    TextInputType type = TextInputType.text,
    bool validate = false,
    FocusNode focusNode}) {
  return Container(
      child: TextFormField(
          maxLength: maxLength,
          focusNode: focusNode,
          textInputAction: TextInputAction.go,
          keyboardType: type,
          maxLines: maxLines,
          controller: controller,
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
              hintText: hint),
          validator: (value) {
            if (validate && value.isEmpty) {
              return 'Vänligen fyll i information här';
            }
            return null;
          }),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12, blurRadius: 10, offset: Offset(0, 2))
          ],
          borderRadius: BorderRadius.circular(13.0)));
}

Widget textFieldWithLabel(TextEditingController controller, String label,
    {String hint,
    int maxLines,
    int maxLength,
    TextInputType type = TextInputType.text,
    bool validate = false,
    FocusNode focusNode}) {
  var field = textField(controller,
      hint: hint,
      maxLines: maxLines,
      maxLength: maxLength,
      type: type,
      validate: validate,
      focusNode: focusNode);
  return label == null
      ? field
      : Column(
          children: [
              Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(label)),
              field,
              Container(height: theme.spaceBtwItems)
            ],
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min);
}

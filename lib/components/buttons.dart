import 'package:catalog/design/theme.dart' as theme;
import 'package:flutter/material.dart';

Widget primaryButton(String text, VoidCallback clickEvent) {
  return _standardBtn(theme.primaryDarkColor, Colors.white, text, clickEvent);
}

Widget secondaryButton(String text, VoidCallback clickEvent) {
  return _standardBtn(
      theme.primaryLightColor, theme.primaryDarkColor, text, clickEvent);
}

Widget _standardBtn(
    Color color, Color textColor, String text, VoidCallback clickEvent) {
  return InkWell(
      child: Container(
          child: Text(text, style: TextStyle(color: textColor)),
          padding: const EdgeInsets.all(15.0),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: color,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, blurRadius: 10, offset: Offset(0, 2))
              ],
              borderRadius: BorderRadius.circular(9.0))),
      onTap: clickEvent);
}

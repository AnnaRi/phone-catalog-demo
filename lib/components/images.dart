import 'package:flutter/material.dart';

Widget profileImage(List<int> imageBytes, double radius) {
  return imageBytes == null
      ? new Container(
          child:
              new Icon(Icons.account_circle, size: radius, color: Colors.grey),
          width: radius,
          height: radius,
          decoration:
              BoxDecoration(color: Colors.white, shape: BoxShape.circle))
      : ClipRRect(
          child: Container(
              child: Image.memory(imageBytes, fit: BoxFit.cover),
              width: radius,
              height: radius),
          borderRadius: BorderRadius.circular(radius));
}

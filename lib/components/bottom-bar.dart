import 'package:flutter/material.dart';

Widget bottomBar(BuildContext context, List<Widget> buttons) {
  return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
          children: List.generate(
              buttons.length,
              (index) => Expanded(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12, horizontal: 8),
                      child: buttons[index]))),
          mainAxisSize: MainAxisSize.max),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(color: Colors.black26, blurRadius: 4, offset: Offset(0, 2))
      ]));
}

library app.theme;

import 'package:flutter/material.dart';

const double fontSizeNormal = 16.0;
const double fontSizeSmall = 15.0;
const double fontSizeLarge = 17.0;
const double fontSizeVeryLarge = 22.0;

const TextStyle baseStyle = TextStyle(fontSize: fontSizeNormal);
const TextStyle subTitleStyle = TextStyle(fontSize: fontSizeNormal);
const TextStyle titleStyle =
    TextStyle(fontSize: fontSizeLarge, fontWeight: FontWeight.bold);

const TextTheme textTheme = TextTheme(
    bodyText1: baseStyle,
    bodyText2: baseStyle,
    button: baseStyle,
    subtitle1: subTitleStyle,
    headline6: TextStyle(fontSize: fontSizeVeryLarge),
    caption: baseStyle);

const double sideMargin = 24.0;
const double spaceBtwItems = 16.0;
EdgeInsetsGeometry pageMargins = EdgeInsets.all(sideMargin);

const Color grey = Color(0xFFBBBDC0);
const Color lightGrey = Color(0xFFc5c5c5);
const Color darkGrey = Color(0xFF3F3F3E);
const Color iconColor = Color(0xFF424242);
const Color lightIconColor = Color(0xFFc1c1c1);
const Color buttonLightGrey = Color(0xFFF9F9F9);

const Color primaryDarkColor = Color(0xFF6B7DB3);
const Color pimaryColor = Color(0xFF9999FF);
const Color primaryLightColor = Color(0xFFE6ECFF);
const Color secondaryColor = Color(0xFFCCD9FF);
const Color alertColor = Color(0xFFf3807d);

final ThemeData theme = ThemeData(
    accentColor: secondaryColor,
    primaryColor: pimaryColor,
    backgroundColor: Colors.white,
    textTheme: textTheme);
